// Based on: https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/822cd9f6a70d2084c31439a4aae2fd78fc3a7dd7/front-end/src/static/TeamInfo.jsx
import SumayaAvatar from "../assets/avatars/sumaya.jpg";
import PeterAvatar from "../assets/avatars/peter.jpeg";
import AdrianAvatar from "../assets/avatars/adrian.jpeg";
import PeytonAvatar from "../assets/avatars/peyton.jpeg";
import NiyatiAvatar from "../assets/avatars/niyati.jpg";

// inspired by TexasVotes project. https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/About/AboutInfo.js

const teamInfo = [
  {
    name: "Sumaya Al-Bedaiwi",
    gitlab_username: "salbedaiwi",
    email: "email",
    image: SumayaAvatar,
    role: "Fullstack",
    bio: "I'm a third-year computer science major with an education certificate. I love embroidery, hanging out with my friends, trying new foods, and all things Star Wars.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
  {
    name: "Peyton Ausburn",
    gitlab_username: "peausburn",
    email: "peausburn@gmail.com",
    image: PeytonAvatar,
    role: "Fullstack",
    bio: "I am a third-year computer science major. In my free time, I enjoy baking, working out, and spending time outdoors.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
  {
    name: "Peter Hwang",
    gitlab_username: "petersh0317",
    email: "petersh0317@utexas.edu",
    image: PeterAvatar,
    role: "Fullstack",
    bio: "I am a third-year Computer Science major. I enjoy practicing judo and working out. I am currently also developing a 3D game.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
  {
    name: "Niyati Prabhu",
    gitlab_username: "niyatiprabhu",
    email: "niyatiprabhu@utexas.edu",
    image: NiyatiAvatar,
    role: "Fullstack",
    bio: "I'm a third-year computer science major and art history minor. I love traveling, weight-lifting, drawing, playing French horn.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
  {
    name: "Adrian Sanchez",
    gitlab_username: "adriansacas",
    email: "adriansc91@utexas.edu",
    image: AdrianAvatar,
    role: "Fullstack",
    bio: "I'm a junior computer science student. I like tennis, soccer, baking, and I am learning how to swim.",
    commits: 0,
    issues: 0,
    unit_tests: 0,
  },
];

export { teamInfo };
