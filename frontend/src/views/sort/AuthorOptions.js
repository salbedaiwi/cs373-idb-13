const AuthorEndpointName = "books";

const AuthorSortOptions = [
  {
    label: "Name",
    db_label: "name",
  },
  {
    label: "DOB",
    db_label: "birth_year"
  }
]

export { AuthorEndpointName, AuthorSortOptions };